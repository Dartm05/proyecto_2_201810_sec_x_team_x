package model.logic;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import API.ITaxiTripsManager;
import model.data_structures.ColaPrioridad;
import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.IList;
import model.data_structures.ILista;
import model.data_structures.Lista;
import model.data_structures.RedBlackBST;
import model.vo.CompaniaTaxi;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	
	private EncadenamientoSeparadoTH<Integer, Lista<Servicio>> tablaArea= new EncadenamientoSeparadoTH<>(200);

	private RedBlackBST<String,Lista<TaxiConServicios>> arbolTaxis= new RedBlackBST<>();
	private EncadenamientoSeparadoTH<String, Lista<Servicio>> tablaDuracion= new EncadenamientoSeparadoTH<>(200);
	private EncadenamientoSeparadoTH<String, RedBlackBST<Taxi,Servicio>> tablacoord= new EncadenamientoSeparadoTH<>(200);
	private RedBlackBST<String,RedBlackBST<String,Servicio>> arbolServicios= new RedBlackBST<>();
	private int numservicioscoord=0;
	
	
	@Override
	public boolean cargarSistema(String direccionJson) {
		try 

		{

		//Crear un InputStream como un new FileInputStream, el parametro que recibe es el link directorio del archivo json

		InputStream inputStream = new FileInputStream(direccionJson);



		//Inicializar un json reader que recibe como parametro el inputstream anteriormente creado

		JsonReader reader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));

		Gson gson = new GsonBuilder().create();
		
		
		reader.beginArray();
		
		

		while (reader.hasNext())

		{

			
		  Servicio servicio= gson.fromJson(reader, Servicio.class);
		  if(servicio.getCompania()!=null)
		 {
		 System.out.println(servicio.getCompania());
		  }
		  else{
		  System.out.println("Independent Owner");
		  }

		  String tripStart= servicio.getStartTime();
		  String tripEnd= servicio.getTripEnd();
		
		  if(tripStart!=null && tripEnd!=null)

			{

				  String[] tripStar= tripStart.split("T");

				  String fechaInicio= tripStar[0];

				  String horaInicio= tripStar[1];
			
	

			  String[] tripEn= tripEnd.split("T");

			  String fechaFinal= tripEn[0];

			  String horaFinal= tripEn[1];
			  
			  RangoFechaHora rangoServicio=  new RangoFechaHora(fechaInicio, fechaFinal, horaInicio, horaFinal);

			  servicio.setRango(rangoServicio);
			
			  Taxi taxiserv= new Taxi();
			  taxiserv.setTaxiId(servicio.getTaxiId());
			  taxiserv.setCompany(servicio.getCompania());
			  Lista<Servicio> serviciostaxi= new Lista<>();
			  serviciostaxi.agregarElementoFinal(servicio);
			  taxiserv.setServiciosTaxi(serviciostaxi);
			  
			  Lista<TaxiConServicios> listaTaxis=new Lista<>();
			  Lista<TaxiConServicios> listaSin= new Lista<>();
			  
			  if(servicio.getCompania()!=null)
			  {
				  TaxiConServicios taxi= new TaxiConServicios(servicio.getTaxiId(),servicio.getCompania());
				  taxi.agregarServicio(servicio);
			
			  if(arbolTaxis.contains(servicio.getCompania()))
			  {
				  listaTaxis=  arbolTaxis.get(servicio.getCompania());
				  listaTaxis.agregarElementoFinal(taxi);
				  arbolTaxis.put(servicio.getCompania(), listaTaxis);
			  }
			  
			  else{
				  Lista<TaxiConServicios> listaTaxisCompania= new Lista<>();
				  listaTaxisCompania.agregarElementoFinal(taxi);
				  arbolTaxis.put(servicio.getCompania(), listaTaxisCompania);
				  
			  }
			  }
			  
			  else if(servicio.getCompania()==null)
			  {
				  
				  TaxiConServicios taxi= new TaxiConServicios(servicio.getTaxiId(),"Independent Owner");
				  taxi.agregarServicio(servicio);

				 
				  if(arbolTaxis.contains("Independent Owner"))
				  {
					listaSin= arbolTaxis.get("Independent Owner");
					listaSin.agregarElementoFinal(taxi);
					arbolTaxis.put("Independent Owner", listaSin);
				  }
				  
				  else{
					  
				  Lista<TaxiConServicios> taxisSin= new Lista<>();
				  taxisSin.agregarElementoFinal(taxi);
				  arbolTaxis.put("Independent Owner", taxisSin);
				  }
			  }
			  
		//	if(servicio.getCompania()==null)
		//	{
		//		System.out.println(arbolTaxis.get("Independent Owner"));
		//	}
		//	else{
		//		System.out.println(arbolTaxis.get(servicio.getCompania()));
		//	}
			
			  
			  Lista<Servicio> ListaServicios= new Lista<>();
			  
			  if(tablaArea.tieneLLave(servicio.getPickupZone()))
			  {
				  Lista<Servicio> listaZona= tablaArea.darValor(servicio.getPickupZone());
				  listaZona.agregarElementoFinal(servicio);
				  tablaArea.insertar(servicio.getPickupZone(), listaZona);
			  }
			  
			  else
			  {
				  ListaServicios.agregarElementoFinal(servicio);
				  tablaArea.insertar(servicio.getPickupZone(), ListaServicios);
			  }
			  
			  
			  
			  //carga 2a
			  
			  Lista<Servicio> serviciosduracion1= new Lista<Servicio>();
			  Lista<Servicio> serviciosduracion2= new Lista<Servicio>();
			  Lista<Servicio> serviciosduracion3= new Lista<Servicio>();
			  Lista<Servicio> serviciosduracion4= new Lista<Servicio>();
			  
			  int duracion= servicio.getTripSeconds();
			  
			  if(duracion>=1 && duracion<=60)
			  {
				  if(tablaDuracion.tieneLLave("grupo 1"))
				  {
					 serviciosduracion1= tablaDuracion.darValor("grupo 1");
					 serviciosduracion1.agregarElementoFinal(servicio);
					 tablaDuracion.insertar("grupo 1", serviciosduracion1);
				  }
				  
				  else{
					  Lista<Servicio> servicio1= new Lista<>();
					  servicio1.agregarElementoFinal(servicio);
					  tablaDuracion.insertar("grupo 1", servicio1); 
				  }
			  }
			  
			  if(duracion>=61 && duracion<=120)
			  {
				  if(tablaDuracion.tieneLLave("grupo 2"))
				  {
					 serviciosduracion2= tablaDuracion.darValor("grupo 2");
					 serviciosduracion2.agregarElementoFinal(servicio);
					 tablaDuracion.insertar("grupo 2", serviciosduracion2);
				  }
				  
				  else{
					  Lista<Servicio> servicio2= new Lista<>();
					  servicio2.agregarElementoFinal(servicio);
					  tablaDuracion.insertar("grupo 2", servicio2); 
				  }
			  }
			  
			  if(duracion>=121 && duracion<=180)
			  {
				  if(tablaDuracion.tieneLLave("grupo 3"))
				  {
					 serviciosduracion3= tablaDuracion.darValor("grupo 3");
					 serviciosduracion3.agregarElementoFinal(servicio);
					 tablaDuracion.insertar("grupo 3", serviciosduracion3);
				  }
				  
				  else{
					  Lista<Servicio> servicio3= new Lista<>();
					  servicio3.agregarElementoFinal(servicio);
					  tablaDuracion.insertar("grupo 1", servicio3); 
				  }
			  }
			  
			  if(duracion>=181)
			  {
				  if(tablaDuracion.tieneLLave("grupo 4"))
				  {
					 serviciosduracion4= tablaDuracion.darValor("grupo 4");
					 serviciosduracion4.agregarElementoFinal(servicio);
					 tablaDuracion.insertar("grupo 4", serviciosduracion4);
				  }
				  
				  else{
					  Lista<Servicio> servicio4= new Lista<>();
					  servicio4.agregarElementoFinal(servicio);
					  tablaDuracion.insertar("grupo 4", servicio4); 
				  }
			  }
			  
			  
			  
			  //carga 2c
			  
			  String latitud= Double.toString(servicio.getPickupLatitud());
			  String longitud= Double.toString(servicio.getPickupLongitud());
			  Lista<Servicio> servicioscoord= new Lista<>();
			  String coord= latitud +","+longitud;
			  
			  //convierte lat/lon a coord cartesiana
			 
			  
			  if(servicio.getPickupLatitud()>=41.0 && servicio.getPickupLongitud()<=-87.0)
			  {
			  double x= Math.cos(servicio.getPickupLatitud())* Math.cos(servicio.getPickupLongitud());
			  double y= Math.cos(servicio.getPickupLatitud())* Math.sin(servicio.getPickupLongitud());
			  double z= Math.sin(servicio.getPickupLatitud());
			  
			  //promedio
			  double promz=0.0;
			  double promx=0.0;
			  double promy=0.0;

			  numservicioscoord++;
			  
			   promx = (promx+x)/numservicioscoord;
			   promy = (promy+y)/numservicioscoord; 
			   promz =(promz+z)/numservicioscoord;
			  
			  
			  //convierte a lat/lon
			  double lon=Math.atan2(promy, promx);
			  double hyp= Math.sqrt(promx*promx + promy*promy);
			  double lat= Math.atan2(promz, hyp);
			  
			  
			  //distanncia harvesiana en millas
			  
			  final double radioTierra= 6371*1000;
			  double latLoc= servicio.getPickupLatitud();
			  double lonLoc= servicio.getPickupLongitud();
		

			  double latDistance= Math.toRadians(lat-latLoc);
			  double lonDistance= Math.toRadians(lon-lonLoc);

			  double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(latLoc)) * Math.cos(Math.toRadians(lat))
			  * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
			  
			  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			  double distance = radioTierra * c ;
			  double distanceTomiles= distance*0.000621371192;
			  distanceTomiles= distanceTomiles-6000;
			  
			  RedBlackBST<Taxi,Servicio> serviciosdistancia1= new RedBlackBST<>();
			  RedBlackBST<Taxi,Servicio> serviciosdistancia2= new RedBlackBST<>();
			  RedBlackBST<Taxi,Servicio> serviciosdistancia3= new RedBlackBST<>();
			  RedBlackBST<Taxi,Servicio> serviciosdistancia4= new RedBlackBST<>();
			  RedBlackBST<Taxi,Servicio> serviciosdistancia5= new RedBlackBST<>();
			  
			  
			  servicio.setDistancetomiles(distanceTomiles);
			  String grupo= "";
			  
			 if(distanceTomiles>=0 && distanceTomiles<0.1)
			 {
				 grupo="grupo 1";
				 if(tablacoord.tieneLLave(grupo))
				 {
					 RedBlackBST<Taxi,Servicio> serviciosllave= tablacoord.darValor(grupo);
					 serviciosllave.get(taxiserv);
					 tablacoord.insertar(grupo, serviciosllave);
				 }
				 else
				 {
					 serviciosdistancia1.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosdistancia1);
				 }
				 
			 }
			 
			 
			 
			 if(distanceTomiles>=0.1 && distanceTomiles<0.2)
			 {
				 grupo= "grupo 2";
				 if(tablacoord.tieneLLave(grupo))
				 {
					 RedBlackBST<Taxi,Servicio> serviciosllave= tablacoord.darValor(grupo);
					 serviciosllave.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosllave);
				 }
				 else
				 {
					 serviciosdistancia2.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosdistancia2);
				 }
				 
			 }
			 if(distanceTomiles>=0.2 && distanceTomiles<0.3)
			 {
				 grupo= "grupo 3";
				 if(tablacoord.tieneLLave(grupo))
				 {
					RedBlackBST<Taxi,Servicio> serviciosllave= tablacoord.darValor(grupo);
					 serviciosllave.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosllave);
				 }
				 else
				 {
					 serviciosdistancia3.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosdistancia3);
				 }
				 
			 }
			 
			 
			 if(distanceTomiles>=0.3 && distanceTomiles<0.4)
			 {
				 grupo= "grupo 4";
				 if(tablacoord.tieneLLave(grupo))
				 {
					 RedBlackBST<Taxi,Servicio> serviciosllave= tablacoord.darValor(grupo);
					 serviciosllave.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosllave);
				 }
				 else
				 {
					 serviciosdistancia4.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosdistancia4);
				 }
				 
			 }
			 
			 
			 else if(distanceTomiles>=0.4)
			 {
				 grupo= "grupo 5";
				 if(tablacoord.tieneLLave(grupo))
				 {
					 RedBlackBST<Taxi,Servicio> serviciosllave= tablacoord.darValor(grupo);
					 serviciosllave.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosllave);
				 }
				 else
				 {
					 serviciosdistancia5.put(taxiserv, servicio);
					 tablacoord.insertar(grupo, serviciosdistancia5);
				 }
			 }
			 if(distanceTomiles>=0.0)
			 { System.out.println(tablacoord.darValor(grupo));
			 System.out.println(distanceTomiles);
			}}

			  
			  //3c
			  
			  
			  String hora= servicio.rango().getHoraInicio();
			  //System.out.println(fecha);
			 // System.out.println(hora);
			  String[] horaar= hora.split(":");
			  String horas= horaar[0];
			  String minutos= horaar[1];
			  String segundos= horaar[2];
			  
			
			  String grupo="";
			  if(horas.equalsIgnoreCase("00"))
			  {
				  grupo="00";
				  
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("01"))
			  {
				  grupo="01";
				  
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("02"))
			  {
				  grupo="02";
				  
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("03"))
			  {
				  grupo="03";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("04"))
			  {
				  grupo="04";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("05"))
			  {
				  grupo="05";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("06"))
			  {
				  grupo="06";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("07"))
			  {
				  grupo="07";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("08"))
			  {
				  grupo="08";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("09"))
			  {
				  grupo="09";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("10"))
			  {
				  grupo="10";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("11"))
			  {
				  grupo="11";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  
			  if(horas.equalsIgnoreCase("12"))
			  {
				  grupo="12";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("13"))
			  {
				  grupo="13";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("14"))
			  {
				  grupo="14";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("15"))
			  {
				  grupo="15";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("16"))
			  {
				  grupo="16";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("17"))
			  {
				  grupo="17";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("18"))
			  { grupo="18";
			  if(arbolServicios.contains(grupo)){
				  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
			  else{
				  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
				  
			  }
			  if(horas.equalsIgnoreCase("19"))
			  {
				  grupo="19";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("20"))
			  {
				  grupo="20";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("21"))
			  {
				  grupo="21";
				  if(arbolServicios.contains(grupo)){
					  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
				  else{
					  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
					  servicioshora1.put(minutos, servicio);
					  arbolServicios.put(grupo, servicioshora1);
				  }
				  
			  }
			  if(horas.equalsIgnoreCase("22"))
			  { grupo="22";
			  if(arbolServicios.contains(grupo)){
				  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
			  else{
				  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
				  
			  }
			  if(horas.equalsIgnoreCase("23"))
			  { grupo="23";
			  if(arbolServicios.contains(grupo)){
				  RedBlackBST<String, Servicio> servicioshora1= arbolServicios.get(grupo); 
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
			  else{
				  RedBlackBST<String, Servicio> servicioshora1=  new RedBlackBST<>();
				  servicioshora1.put(minutos, servicio);
				  arbolServicios.put(grupo, servicioshora1);
			  }
			  
			  }
			  
			 // System.out.println(arbolServicios.get(grupo));
			  
			  
			  
			  
		}}
		reader.close();
		}
					catch (UnsupportedEncodingException ex) 

				{

				System.out.println(ex.getMessage());

				}

				catch (IOException ex) 

				{

				System.out.println(ex.getMessage());

				} catch(IllegalStateException ex)
				{
					
				}
		
		return true;
	}

	
	@Override
	public ILista<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		

		Lista<TaxiConServicios> listataxis= new Lista<>();
		TaxiConServicios taximas= null;
		int numServiciosmas=0;
		
		
		RedBlackBST<Integer, TaxiConServicios> arbolTaxisret= new RedBlackBST<>();
		
		if(tablaArea.tieneLLave(zonaInicio))
		{
			Lista<Servicio> servicioszona= tablaArea.darValor(zonaInicio);
			for(int n=0;n<servicioszona.darNumeroElementos();n++)
			{
				Servicio actual= servicioszona.darElemento(n);
				
				//System.out.println(actual);)
				
					if(arbolTaxis.contains(compania))
					{
						Lista<TaxiConServicios> taxi= arbolTaxis.get(compania);
						
						for(int i=0;i<taxi.darNumeroElementos();i++)
						{
							TaxiConServicios taxiac= taxi.darElemento(i);
							
							if(taxiac.getTaxiId().equalsIgnoreCase(actual.getTaxiId()))
							{
								taxiac.agregarServicio(actual);
						
							
							if(taxiac.numeroServicios()>=numServiciosmas)
							{
								numServiciosmas= taxiac.numeroServicios();
								arbolTaxisret.put(taxiac.numeroServicios(), taxiac);
							}
							}
						}
				}
				else{
					System.out.println("No tiene servicios");
				}
			
			}
			}
		
		int max= arbolTaxisret.max();
		taximas= arbolTaxisret.get(max);
		TaxiConServicios taxiret= null;
		
		for(int i=0;i<arbolTaxisret.size();i++)
		{
				
				if(arbolTaxisret.max()>=max)
				{
					max= arbolTaxisret.max();
					taxiret= arbolTaxisret.get(max);
					listataxis.agregarElementoFinal(taxiret);
					arbolTaxisret.deleteMax();
				}
			
		}
			
	
		return  listataxis;
	}


	@Override
	public ILista<Servicio> A2ServiciosPorDuracion(int duracion) {
		
		Lista<Servicio> listaServicios=  new Lista<Servicio>();
		String grupo= null;
		
		if(duracion>=1 && duracion<=60)
		{
			grupo= "grupo 1";
			listaServicios=tablaDuracion.darValor(grupo);
		}
		
		if(duracion>=61 && duracion<=120)
		{
			grupo= "grupo 2";
			listaServicios=tablaDuracion.darValor(grupo);
		}
		
		if(duracion>=121 && duracion<=180)
		{
			grupo= "grupo 3";
			listaServicios=tablaDuracion.darValor(grupo);
		}
		else if(duracion>=181)
		{
			grupo= "grupo 4";
			listaServicios=tablaDuracion.darValor(grupo);
		}
		
		return listaServicios;
	}


	@Override
	public ILista<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public ILista<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		
		Lista<Servicio> listaServiciosret= new Lista<>();
		
		String grupo="";
		if(millasReq2C>=0 && millasReq2C<0.1)
		{
			grupo="grupo 1";
		}
		
		
		if(millasReq2C>=0.1 && millasReq2C<0.2)
		{
			grupo="grupo 2";
		}
		
		
		
		if(millasReq2C>=0.2 && millasReq2C<0.3)
		{
			grupo="grupo 3";
		}
		
		
		if(millasReq2C>=0.3 && millasReq2C<0.4)
		{
			grupo="grupo 4";
		}
		
		else if(millasReq2C>=0.4)
		{
			grupo="grupo 5";
		}
		
		
		RedBlackBST<Taxi, Servicio> taxis= tablacoord.darValor(grupo);
		//System.out.println(taxis.size());
		boolean taxi= false;
		
		while(!taxis.isEmpty() && !taxi)
		{
			Taxi actual=taxis.max();
			String idactual= actual.getTaxiId();
			//System.out.println(idactual);
			
			if(idactual.equalsIgnoreCase(taxiIDReq2C))
			{
				listaServiciosret=actual.getServiciosTaxi();
				//System.out.println(listaServiciosret);
				taxi=true;
			}
			
			taxis.deleteMax();
		}
		
		return listaServiciosret;
	}

	@Override
	public ILista<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		
		Lista<Servicio> listaretorno=  new Lista<Servicio>();
		String[] horas= hora.split(":");
		String horra= horas[0];
		
		if(arbolServicios.contains(horra))
		{
			RedBlackBST<String, Servicio> servicios= arbolServicios.get(horra);
			//System.out.println(servicios);
			
		
			while(!servicios.isEmpty())
			{
				String max= servicios.max();
				Servicio servicioactual= servicios.get(max);
				if(servicioactual.rango().getFechaInicial().equalsIgnoreCase(fecha))
				{
					if(servicioactual.getPickupZone() != servicioactual.getDropOffZone())
					{
					listaretorno.agregarElementoFinal(servicioactual);
					}
				}
				servicios.deleteMax();
			}
			
		}
		
		return listaretorno;
	}




}
