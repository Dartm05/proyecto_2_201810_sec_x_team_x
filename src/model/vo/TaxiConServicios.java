package model.vo;

import model.data_structures.IList;
import model.data_structures.ILista;
import model.data_structures.*;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private ILista<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios = new Lista<Servicio>(); // inicializar la lista de servicios 
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public ILista<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.darNumeroElementos();
    }

    public void agregarServicio(Servicio servicio){
        servicios.agregarElementoFinal(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
    			if(this.numeroServicios()> o.numeroServicios())
    			{
    				return 1;
    			}
    			if(this.numeroServicios()< o.numeroServicios())
    			{
    				return -1;
    			}
    			else
    			{
    				return 0;
    			}
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        for(Servicio s : servicios){
            System.out.println("\t"+s.getStartTime());
        }
        System.out.println("___________________________________");;
    }
}
