package model.vo;
import model.data_structures.Lista;
import model.data_structures.ListaDobleEncadenada;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String taxi_id;
	private String company;
	private Lista<Servicio> serviciosTaxi;
	//private InfoTaxiRango infoTaxi;
	//private ServiciosValorPagado serviciosvalor;
	
	public Taxi()
	{
		
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
	
		return taxi_id;
	}
	
	
	public void setTaxiId(String nuevo) {
		this.taxi_id= nuevo;

	}

	/**
	 * @return company
	 */
	public void setCompany(String comp) {
		this.company= comp;

	}
	
	
	
	public String getCompany() {
	
		return  company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		
		if(this.getTaxiId().equals(o.getTaxiId()))
		{
			return 0;
		}
		
		else if( this.getTaxiId().compareTo(o.getTaxiId())>0)
		{
			return 1;
		}
		else 
		{
			return -1;
		}
		
	}

	public Lista<Servicio> getServiciosTaxi() {
		return serviciosTaxi;
	}

	public void setServiciosTaxi(Lista<Servicio> serviciosTaxi) {
		this.serviciosTaxi = serviciosTaxi;
	}

	//public InfoTaxiRango getInfoTaxi() {
//		return infoTaxi;
//	}

//	public void setInfoTaxi(InfoTaxiRango infoTaxi) {
	//	this.infoTaxi = infoTaxi;
//	}

//	public ServiciosValorPagado getServiciosvalor() {
//		return serviciosvalor;
//	}

//	public void setServiciosvalor(ServiciosValorPagado serviciosvalor) {
//		this.serviciosvalor = serviciosvalor;
//	}	
}