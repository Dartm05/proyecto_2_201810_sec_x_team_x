package model.vo;

public class CompaniaTaxi implements Comparable<CompaniaTaxi>{
	
	private String nomCompania;
	private int totalServicios;
	private TaxiConServicios taxi;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public TaxiConServicios getTaxi() {
		return taxi;
	}

	public void setTaxi(TaxiConServicios taxi) {
		this.taxi = taxi;
	}

	@Override
	public int compareTo(CompaniaTaxi o) {
		if(this.totalServicios > o.getTotalServicios())
		{
			return 1;
		}
		
		if(this.totalServicios == o.getTotalServicios())
		{
			return 0;
		}
		else
		{
			return -1;
		}

	
	}

	public int getTotalServicios() {
		return totalServicios;
	}

	public void setTotalServicios(int totalServicios) {
		this.totalServicios = totalServicios;
	}
	
	

}
