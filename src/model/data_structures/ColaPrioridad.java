package model.data_structures;

public class ColaPrioridad < T extends Comparable<T>>  {
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	private T[] cola;
	
	private int size=0;
	
	private int maxSize=0;
	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	/**
	 * crea una cola de prioridad con el tama�o dado por parametro
	 * @param int max el tama�o inicial de la cola
	 */
	public ColaPrioridad(int max) {

		cola= (T[]) (new Comparable[max]);
		size=0;
		maxSize=max;
	}
	/**
	 * @return el numero de elementos que tiene la cola de prioridad
	 */
	public int darNumElementos()
	{
		return size;
	}
	/**
	 * agrega un elementoo a la cola de prioridad
	 * @param T elemento el elemento a a�adir
	 */
	public void agregar(T elemento)
	{	
		if (size==maxSize) {
			throw new StackOverflowError();
		}
		int i = size-1;
		while (i >= 0 && less(elemento, cola[i])) 
		{
			cola[i+1] = cola[i];
			i--;
		}
		cola[i+1] = elemento;
		size++;
	}
	/**
	 * @return el elemento de mayor prioridad en la cola
	 */
	public T max()
	{
		T x = cola[size-1];
		cola[size-1] = null;
		size--;
		return x;
	}

	/**
	 * @return el estado de la cola, si est� vacia o no.
	 */
	public boolean isEmpty()
	{
		return size==0;
	}
	/**
	 * @return el numero de elementos en la cola.
	 */
	public int maxSize()
	{
		return maxSize;
	}

	//M�TODOS ADICIONALES
	/**
	 * @param T x, T y los elementos a comparar
	 * @return si el elemento es mayor al otro
	 */
	private boolean less(T x, T y)
	{
		return x.compareTo(y) < 0;

	}

}
