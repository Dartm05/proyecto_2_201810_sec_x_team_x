package model.data_structures;

import java.util.Iterator;

public class Lista<T> implements ILista<T> {
	// -------------------------------------------------------------
	// Clase interna Nodo Simple.
	// -------------------------------------------------------------

	private class NodoSimple<T> {
		/**
		 * siguiente nodo
		 */
		NodoSimple<T> next;
		/**
		 * item del nodo.
		 */
		T item;
		/**
		 * metodo contructor del nodo.
		 */
		public NodoSimple() {
			next = null;
			item = null;
		}

	}

	// -------------------------------------------------------------
	// Clase principal lista.
	// -------------------------------------------------------------
	/**
	 * referencia a la cabeza de la lista.
	 */
	private NodoSimple<T> head;
	/**
	 * referencia al ultimo elemento de la lista.
	 */
	private NodoSimple<T> last;
	/**
	 * tamanio de la lista.
	 */
	private int size;
	
	private NodoSimple<T> sorted;

	/**
	 * Metodo constructor, inicializa la cabeza de la lista en null.
	 */
	public Lista() {
		head = null;
		last = head;
	}

	/**
	* metodo que retorna un iterador sobre los elementos de la lista.
	*/
	public Iterator<T> iterator() {

		return new Iterator<T>() {

			private NodoSimple<T> actual = head;

			public boolean hasNext() {
				return actual != null;
			}

			public T next() {
				T item = actual.item;
				actual = actual.next;
				return item;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}

	/**
	 * agrega un elemento al inicio de la lista
	 * 
	 * @param elem elemento a agregar.
	 */
	public void agregarElementoInicio(T elem) {
		NodoSimple<T> nuevo = new NodoSimple<T>();
		nuevo.item = elem;
		if (head == null) {
			head = nuevo;
			last = head;
		} else {
			nuevo.next = head;
			head = nuevo;
		}
		size++;
	}

	/**
	 * agraga un elemento al final de la lista.
	 * 
	 * @param elem elemento a agregar.
	 */
	public void agregarElementoFinal(T elem) {
		NodoSimple<T> newNode = new NodoSimple<T>();
		newNode.item = elem;
		if (head == null) {
			head = newNode;
			last = head;
		} else {
			last.next = newNode;
			last = newNode;

		}
		size++;
	}

	/**
	 * @return el elemento en la lista segun la posicion dada por parametro
	 * @param pos posicion que se desa encontrar
	 */
	public T darElemento(int pos) {
		if (pos < 0 || pos >= size) {
			throw new ArrayIndexOutOfBoundsException(pos);
		} else if (pos == 0) {
			return head.item;
		} else {
			NodoSimple<T> recorrido = head;
			for (int newPos = 0; newPos < pos; newPos++)
				recorrido = recorrido.next;
			return recorrido.item;
		}
	}

	/**
	 * elimina el primer elemento de la lista.
	 * @return el primer elemento de la lista eliminado.
	 */
	public T eliminarPrimero() {
		if (head == null)
			throw new NullPointerException();
		T item = head.item;
		head = head.next;
		size--;
		if (size == 0) {
			head = null;
		}
		return item;

	}

	/**
	 * @return el tamanio de la lista.
	 */
	public int darNumeroElementos() {
		return size;
	}

	/**
	 * retorna el ultimo elemento de la lista.
	 * @return ultimo elemento de la lista.
	 */
	public T darUltimoElemento() {
		return last.item;
	}

	/**
	 * retorna el primero elemento de la lista.
	 * @return primero elemento de la lista.
	 */
	public T darPrimerElemento() {
		return head.item;
	}
	
	/**
	 * elimina el elemento de la lista segun la posicion indicada
	 * @return ultimo elemento de la lista.
	 */
	// TODO Documentar.
	
	public T eliminarElemento(int pos) {
		if (head == null)
			throw new NullPointerException();
		else {
			NodoSimple<T> current = head;
			for (int newPos = 0; newPos <= pos - 1; newPos++)
				current = current.next;
			T rta = current.next.item;
			current.next = current.next.next;
			size--;
			return rta;
		}
	}

	// void insertionSort(T headref) 
	 //   {
	        // Initialize sorted linked list
	   //     sorted = null;
	  //      NodoSimple<T> current = new NodoSimple<>();
	   //     current.item= headref;
	        // Traverse the given linked list and insert every
	        // node to sorted
	   //     while (current != null) 
	    //    {
	            // Store next for next iteration
	    //        NodoSimple<T> next = current.next;
	            // insert current in sorted linked list
	    //        sortedInsert(current);
	            // Update current
	     //       current = next;
	   //     }
	        // Update head_ref to point to sorted linked list
	    //    head = sorted;
	   // }
	 
	 
	// void sortedInsert(NodoSimple<T> newnode) 
	  //  {
	        /* Special case for the head end */
	   //     if (sorted == null || sorted.item.compareTo(newnode.item)>0) 
	    //    {
	    //        newnode.next = sorted;
	    //        sorted = newnode;
	     //   }
	   //     else
	    //    {
	    //        NodoSimple<T> current = sorted;
	            /* Locate the node before the point of insertion */
	     //       while (current.next != null && current.next.item.compareTo(newnode.item)<0) 
	     //       {
	      //          current = current.next;
	       //     }
	     //       newnode.next = current.next;
	     //       current.next = newnode;
	     //   }
	  //  }
	 

}
