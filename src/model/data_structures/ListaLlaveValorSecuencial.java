package model.data_structures;

public class ListaLlaveValorSecuencial<K , V>
{
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------
	private int tamanio;

	private SimpleNode<V, K> head;
	
	private SimpleNode<V,K> last;

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------

//	public Iterator<K> iterator() {
//		// TODO Auto-generated method stub
//		return new Iterator<K>() {
//
//			SimpleNode<V, K> actual= head;
//
//			public boolean hasNext() {
//				// TODO Auto-generated method stub
//				if(tamanio==0)
//					return false;
//				if(actual==null)
//					return false;
//
//				return actual!=null;
//			}
//
//			public K next() {
//				// TODO Auto-generated method stub
//				K item = actual.getLlave();
//				actual= actual.getNext();
//				return item;
//			}
//		};
//	}
	/**
	 * inicializa la lista de llave valor secuencial
	 */
	public ListaLlaveValorSecuencial()
	{
		tamanio = 0;
		head = null;
	}
	/**
	 * @return el tamanio de la lista de valor secuencial
	 */
	public int darTamanio()
	{
		return tamanio;
	}
	/**
	 * @return el estado de la lista, si esta vacia o no
	 */
	public boolean estaVacia()
	{
		if(tamanio == 0)
			return true;
		else
			return false;
	}
	/**
	 * @param K pllave, la llave del objeto
	 * @return si la llave aparece e la lista de llave valor secuencial
	 */
	public boolean tieneLlave(K pllave)
	{
		if (head != null) 
		{
			SimpleNode<V, K> actual = head;			
			while (actual != null) 
			{
				if (actual.getLlave().equals(pllave))
					return true;

				actual = actual.getNext();
			}
		}
		return false;
	}
	
	/**
	 * da el valor del objeto asociado a la llave ingresada por parametro
	 * @param K pllave, la llave asociada a el objeto
	 * @return valor del objeto
	 */
	public V darValor(K pllave)
	{
		V miValor = null;

		SimpleNode<V, K> actual = head;
		while(actual != null && miValor == null)
		{
			if (actual.getLlave().equals(pllave))
				miValor = actual.getValor();

			actual = actual.getNext();
		}

		return miValor;
	}

	/**
	 * inserta un objeto en la lista lllave valor secuencial 
	 *  @param K pllave, V pvalor, la llave y el valor del objeto a insertar
	 */
	public void insertar(K pllave, V pValor) //completar
	{
		SimpleNode<V, K> nuevo = new SimpleNode<V,K>(pValor, pllave);
		//Caso 1 no hay nodos en la lista
		if(head==null)
		{
			head=nuevo;
			last=head;
			tamanio++;
		}
		//Caso 2 el valor de parametro es null y la llave existe por lo tanto se elimina el nodo 
		if(pValor==null)
		{
			eliminarSegunLlave(pllave);
		}

		//Hago una variable existe para optimizar
		boolean yaExiste =tieneLlave(pllave);

		//Caso 3 la llave existe y el valor del parametro no es null, por lo tanto modifico el valor del  nodo
		if(yaExiste)
		{
			darElementoDeLlaveIgualParaInsertar(pllave).setValor(pValor);
		}
		//Caso 4 la llave no existe y el valor del parametro no es  null, entonces agrego a la lista
		if(!yaExiste)
		{
			agregarElementoFinal(pllave, pValor);
		}

	}
	/**
	 * agrega el elemento en una lista de llave valor secuencia
	 *  @param K pllave, V pvalor, la llave y el valor del objeto a agregar
	 */
	public void agregarElementoFinal(K pllave, V pValor) 
	{
		SimpleNode<V, K> actual= head;
		SimpleNode<V, K> newNode= new SimpleNode<V, K>(pValor, pllave);
		if (head == null) {
			head = newNode;
			last = head;
		} else {
			last.setNext(newNode);
			last = last.getNext();

		}
		tamanio++;
	}
	/**
	 * elimina un objeto en la lista llave valor secuencial
	 *  @param K pllave, V pvalor, la llave y el valor del objeto a insertar
	 */
	public void eliminarSegunLlave(K pllave)
	{
		SimpleNode<V, K> actual = head;
		boolean termino = false;

		if (actual != null) 
		{
			if(tamanio == 1 && actual.getLlave().equals(pllave))
			{
				actual = null;
				tamanio--;
				termino=true;
			}
			if(head.getLlave().equals(pllave))
			{
				head= head.getNext();
				tamanio--;
				termino=true;
			}
			else
				while (actual.getNext() != null && !termino) 
				{
					if (actual.getNext().getLlave().equals(pllave)) 
					{
						actual.setNext(actual.getNext().getNext());
						actual.getNext().setNext(null);
						termino = true;
						tamanio--;
					}

					actual = actual.getNext();
				}
			//modifique aqui 
//			if(termino==false)
//				System.out.println("No se elimino elemento con esa llave porque no existe");
		}
		else
			System.out.println("No hay elementos para eliminar");


	}

	//M�TODOS ADICIONALES
	/**
	 *Metodo que retorna el nodo que tiene la llave igual a la ingresada por parametro para
	 *hacer mas facil el tercer caso del metodo insertar
	 */
	public SimpleNode darElementoDeLlaveIgualParaInsertar(K pllave)
	{
		if (head != null) 
		{
			SimpleNode<V, K> actual = head;			
			while (actual != null) 
			{
				if (actual.getLlave().equals(pllave))
					return actual;

				actual = actual.getNext();
			}
		}
		return null;
	}
	public Iterable<K> keys()  {
        Lista<K> lista = new Lista<K>();
        for (SimpleNode<V, K> x = head; x != null; x = x.getNext())
        	lista.agregarElementoFinal(x.getLlave());
        return lista;
    }

}