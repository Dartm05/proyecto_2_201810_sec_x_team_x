package model.data_structures;

import java.util.Iterator;

public class Cola<K extends Comparable<K>> implements IQueue<K>  {

	
	private int elementos;
	public Lista<K> lista;
	
	public Cola()
	{
		elementos=0;
		lista= new Lista<K>();
	}
	
	public int size()
	{
		return elementos;
	}
	
	public void enqueue (K item) {
		
		lista.agregarElementoFinal(item);
		elementos++;
	}

	
	public K dequeue() {
		
		elementos--;
		return lista.eliminarPrimero();
		
	}

	
	public boolean isEmpty() {
		
		return lista.darNumeroElementos()==0;
	}

	
	public Iterator<K> iterator() {
		
		return lista.iterator();
	}

	public K peek() {
			return lista.darPrimerElemento();
	}

}
