package model.data_structures;

import java.util.Iterator;

public class EncadenamientoSeparadoTH<K, V>  {
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------
	private static final int TAMANIO = 4;

	private int n;         

	private int m;   

	private ListaLlaveValorSecuencial<K, V>[] st;

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	/**
	 * inicializa una lista de llave valor secuencial dado un tama�o dado
	 * @param int m el tama�o inicial de la lista
	 */
	public EncadenamientoSeparadoTH(int m) {
		this.m = m;
		st = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[m];
		for (int i = 0; i < m; i++)
			st[i] = new ListaLlaveValorSecuencial<K, V>();
	} 
	/**
	 * redimensiona el tama�o de la lista de llave valor secuencial
	 * @param int chains el nuevo tama�o de la lista
	 */
	private void resize(int chains) {
		EncadenamientoSeparadoTH<K, V> temp = new EncadenamientoSeparadoTH<K, V>(chains);
		for (int i = 0; i < m; i++) {
			for (K key : st[i].keys()) {
				temp.insertar(key, st[i].darValor(key));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.st = temp.st;
	}
	
	/**
	 * @param k key la llave del objeto
	 * @return la funcion de hash para ingresar el objeto a la lista
	 */
	private int hash(K key) {
		return (key.hashCode() & 0x7fffffff) % m;
	} 
	
	/**
	 * @return el tama�o de la lista de llave valor secuencial
	 */
	public int darTamanio() {
		return n;
	} 
	/**
	 * realiza la funcion de hash
	 * @param k key la llave del objeto
	 * @return la funcion de hash para ingresar el objeto a la lista
	 */
	public boolean estaVacia() {
		return darTamanio() == 0;
	}

	/**
	 * @param k key la llave del objeto
	 * @return si la lista llave valor secuencial tiene la llave ingresada por parametro
	 */
	public boolean tieneLLave(K key) {
		if (key == null) throw new IllegalArgumentException("La llave ingresada es null");
		return darValor(key) != null;
	} 

	/**
	 * @param k key la llave del objeto
	 * @return el valor del objeto en la lista llave valor secuencial
	 */
	public V darValor(K key) {
		if (key == null) throw new IllegalArgumentException("La llave ingresada es null");
		int i = hash(key);
		return st[i].darValor(key);
	} 
	/**
	 * inserta en la lista llave valor secuencial un objeto
	 * @param k key la llave del objeto, V val el valor del objeto
	 */
	public void insertar(K key, V val) {
		if (key == null) throw new IllegalArgumentException("El valor ingresado es null");
		if (val == null) {
			eliminar(key);
			return;
		}

		if (n >= 10*m) resize(2*m);

		int i = hash(key);
		if (!st[i].tieneLlave(key)) n++;
		st[i].insertar(key, val);
	} 
	/**
	 * elimina un objeto de la lista llave valor separado dada su llave
	 * @param k key la llave del objeto
	 */
	public void eliminar(K key) {
		if (key == null) throw new IllegalArgumentException("La llave a eliminar es null");

		int i = hash(key);
		if (st[i].tieneLlave(key)) n--;
		st[i].eliminarSegunLlave(key);

		if (m > TAMANIO && n <= 2*m) resize(m/2);
	} 
	/**
	 * itera sobre las llaves de la lista llave valor secuencial
	 * @return una lista con las llaves de la lista llave valor secuencial
	 */
	public Iterable<K> llaves() {
		Lista<K> lista = new Lista<K>();
		for (int i = 0; i < m; i++) {
			for (K key : st[i].keys())
				lista.agregarElementoFinal(key);
		}
		return lista;
	} 
}
