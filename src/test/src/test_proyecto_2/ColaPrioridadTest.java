package test.src.test_proyecto_2;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;

public class ColaPrioridadTest extends TestCase{

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	ColaPrioridad<String> cola= new ColaPrioridad<>(8);

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		cola.agregar("String test 1");
		cola.agregar("String test 2");
		cola.agregar("String test 3");
		cola.agregar("String test 4");
		cola.agregar("String test 5");
		cola.agregar("String test 6");
		cola.agregar("String test 7");
	}

	public void testDarNumeroDeElementos(){
		
		assertEquals("Al principio no deben haber elementos", 0, cola.darNumElementos());
		
		setupEscenario1( );
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 7, cola.darNumElementos());
	}
	
	public void testAgregar(){
		
		cola.agregar("String test 1");
		
		cola.agregar("String test 2");
		
		cola.agregar("String test 3");
		
		cola.agregar("String test 4");
		
		cola.agregar("String test 5");
		
		cola.agregar("String test 6");
		
		cola.agregar("String test 7");
		
	}

	public void testMax(){
		
		cola.agregar("String test 0");
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 0", cola.max());
		
		setupEscenario1( );

		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", cola.max());
		
		
	}
	
	public void testMaxSize(){
		
		assertEquals("El tama�o no es el correcto", 8, cola.maxSize());
		
		cola= new ColaPrioridad<>(5);

		assertEquals("El tama�o no es el correcto", 5, cola.maxSize());
		
	}

}
