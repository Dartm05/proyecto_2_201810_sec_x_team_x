package test.src.test_proyecto_2;

import junit.framework.TestCase;
import model.data_structures.*;

public class ListaLlaveValorSecuencialTest extends TestCase  {

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	ListaLlaveValorSecuencial<Integer, String> lista = new ListaLlaveValorSecuencial<>();

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		lista.agregarElementoFinal(123, "String 1");
		lista.agregarElementoFinal(456, "String 2");
		lista.agregarElementoFinal(789, "String 3");
		lista.agregarElementoFinal(147, "String 4");
		lista.agregarElementoFinal(258, "String 5");
	}

	public void testDarTamanio(){
		
		assertEquals("Al inicio debe ser cero", 0, lista.darTamanio());
		
		setupEscenario1( );
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 5, lista.darTamanio());
	}

	public void testEstaVacia(){

		assertTrue("Al principio el encadenamiento debe estar vacio", lista.estaVacia());
		
		setupEscenario1( );
		
		assertFalse("Ahora el encadenamiento no debe estar vacio", lista.estaVacia());
	}

	public void testTieneLLave(){
		
		setupEscenario1();
		
		assertTrue("El encadenamiento debe tener esa llave", lista.tieneLlave(789));
		
		assertTrue("El encadenamiento debe tener esa llave", lista.tieneLlave(123));
		
		assertTrue("El encadenamiento debe tener esa llave", lista.tieneLlave(456));
		
		assertFalse("El encadenamiento NO debe tener esa llave", lista.tieneLlave(951));
	}
	
	public void testDarValor(){
		
		setupEscenario1( );
		
		assertEquals("El valor es incorrecto", "String 3", lista.darValor(789));
		
		assertEquals("El valor es incorrecto", "String 1", lista.darValor(123));
		
		assertEquals("El valor es incorrecto", "String 2", lista.darValor(456));
	}
	
	public void testInsertar(){
		
		assertTrue("Al principio el encadenamiento debe estar vacio", lista.estaVacia());
		
		lista.agregarElementoFinal(117, "StringInsertado");
		
		assertFalse("Ahora el encadenamiento no debe estar vacio", lista.estaVacia());
		
		assertEquals("El string no es el correcto", "StringInsertado", lista.darValor(117));
	}
	
	public void testEliminarSegunLLave(){
		
		setupEscenario1();
		
		assertEquals("El tama�o ahora debe ser 5", 5, lista.darTamanio());
		
		lista.eliminarSegunLlave(123);
		
		assertEquals("El tama�o ahora debe ser 4", 4, lista.darTamanio());
		
		assertFalse("El encadenamiento ahora NO debe tener esa llave", lista.tieneLlave(123));
		
		lista.eliminarSegunLlave(456);
		
		assertEquals("El tama�o ahora debe ser 3", 3, lista.darTamanio());
		
		assertFalse("El encadenamiento ahora NO debe tener esa llave", lista.tieneLlave(456));
	}

}
