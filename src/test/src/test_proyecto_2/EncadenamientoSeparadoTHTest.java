package test.src.test_proyecto_2;

import junit.framework.TestCase;
import model.data_structures.Cola;
import model.data_structures.EncadenamientoSeparadoTH;

public class EncadenamientoSeparadoTHTest extends TestCase {
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	EncadenamientoSeparadoTH<Integer, String> encadenamiento = new EncadenamientoSeparadoTH<>(5);

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		encadenamiento.insertar(123, "String1");
		encadenamiento.insertar(654, "String2");
		encadenamiento.insertar(789, "String3");
	}

	public void testDarNumeroDeElementos(){
		
		setupEscenario1( );
		
		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 3, encadenamiento.darTamanio());
	}

	public void testIsEmpty(){

		assertTrue("Al principio el encadenamiento debe estar vacio", encadenamiento.estaVacia());
		
		setupEscenario1( );
		
		assertFalse("Ahora el encadenamiento no debe estar vacio", encadenamiento.estaVacia());
	}
	
	public void testTieneLLave(){
		setupEscenario1();
		assertTrue("El encadenamiento debe tener esa llave", encadenamiento.tieneLLave(654));
		
		assertTrue("El encadenamiento debe tener esa llave", encadenamiento.tieneLLave(123));
		
		assertTrue("El encadenamiento debe tener esa llave", encadenamiento.tieneLLave(789));
		
		assertFalse("El encadenamiento NO debe tener esa llave", encadenamiento.tieneLLave(147));
	}

	public void testInsertarConDarValor(){
		
		assertTrue("Al principio el encadenamiento debe estar vacio", encadenamiento.estaVacia());
		
		encadenamiento.insertar(117, "StringInsertado");
		
		assertFalse("Ahora el encadenamiento no debe estar vacio", encadenamiento.estaVacia());
		
		assertEquals("El string no es el correcto", "StringInsertado", encadenamiento.darValor(117));
	}
	
	public void testEliminar(){
		
		setupEscenario1();
		
		assertEquals("El numero de elementos debe ser 3", 3, encadenamiento.darTamanio());
		
		encadenamiento.eliminar(123);
		
		assertEquals("No se eliminaron los elementos correctamente o esta mal el contador", 2, encadenamiento.darTamanio());
	}
	
	
}
