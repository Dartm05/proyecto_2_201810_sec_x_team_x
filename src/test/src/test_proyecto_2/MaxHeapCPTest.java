package test.src.test_proyecto_2;

import java.util.NoSuchElementException;

import junit.framework.TestCase;
import model.data_structures.ColaPrioridad;
import model.data_structures.MaxHeapCP;

public class MaxHeapCPTest extends TestCase {
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	MaxHeapCP<String> MHCP= new MaxHeapCP<>();;

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	public void setupEscenario1( )
	{
		MHCP.insert("String test 1");
		MHCP.insert("String test 2");
		MHCP.insert("String test 3");
		MHCP.insert("String test 4");
		MHCP.insert("String test 5");
		MHCP.insert("String test 6");
		MHCP.insert("String test 7");
	}

	public void testEmpty(){

		assertTrue("Al principio la cola debe estar vacia", MHCP.isEmpty());

		setupEscenario1();

		assertFalse("La cola ahora no debe estar vacia", MHCP.isEmpty());
	}


	public void testDarNumeroDeElementos(){

		assertEquals("Al principio no deben haber elementos", 0, MHCP.size());

		setupEscenario1( );

		assertEquals("No se agregaron los elementos correctamente o esta mal el contador", 7, MHCP.size());
	}


	public void testMax(){

		MHCP.insert("String test 0");

		assertEquals("Los elementos no tienen el orden que se supone", "String test 0", MHCP.max());
		
		setupEscenario1( );

		assertEquals("Los elementos no tienen el orden que se supone", "String test 7", MHCP.max());

		MHCP.insert("String test 8");

		assertEquals("Los elementos no tienen el orden que se supone", "String test 8", MHCP.max());
	}

	public void testInsert(){

		MHCP.insert("String test 1");

		MHCP.insert("String test 2");

		MHCP.insert("String test 3");

		MHCP.insert("String test 4");

		MHCP.insert("String test 5");

		MHCP.insert("String test 6");

		MHCP.insert("String test 7");
	}

	public void testDeleteMax(){

		setupEscenario1( );
		
		MHCP.delMax();
		assertEquals("Los elementos no tienen el orden que se supone", "String test 6", MHCP.max());
		
		MHCP.delMax();
		assertEquals("Los elementos no tienen el orden que se supone", "String test 5", MHCP.max());
		
		MHCP.delMax();
		assertEquals("Los elementos no tienen el orden que se supone", "String test 4", MHCP.max());
		
		MHCP.delMax();	MHCP.delMax();	MHCP.delMax();
		
		assertEquals("Los elementos no tienen el orden que se supone", "String test 1", MHCP.max());
	}

}
